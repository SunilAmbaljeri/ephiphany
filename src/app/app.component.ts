import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Ephiphany';
  public options = {
    position: ["top", "right"],
    lastOnBottom: true,
    animate: 'fromRight',
    timeOut: 2000,
    showProgressBar: true,
    clickToClose: false
  };
  constructor() { 
  }

  ngOnInit(): void {
 
  }
}
