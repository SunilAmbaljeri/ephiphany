import { TestBed } from '@angular/core/testing';

import { AppWebUrlsService } from './app-web-urls.service';

describe('AppWebUrlsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppWebUrlsService = TestBed.get(AppWebUrlsService);
    expect(service).toBeTruthy();
  });
});
