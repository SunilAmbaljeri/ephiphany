import { TestBed } from '@angular/core/testing';

import { ErrortransformService } from './errortransform.service';

describe('ErrortransformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErrortransformService = TestBed.get(ErrortransformService);
    expect(service).toBeTruthy();
  });
});
