import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrortransformService {

  constructor() { }
  // GlobalFunction Class
  transformError(err) {
    if(err instanceof HttpErrorResponse){
      return {
        status: 504,
        action: 1,
        message: 'Something went wrong, Connection Refused'
      }
  } else {
    switch (err.status) {
      case 400:
        return {
          status: err.status,
          message: 'It looks like Bad Request'
        }
      case 401:
        return {
          status: err.status,
          message: 'It looks like the resource has not been authenticated'
        }
      case 403:
        return {
          status: err.status,
          message: 'It looks like that resource is forbidden'
        }
      case 404:
        return {
          status: err.status,
          message: 'It looks like that resource was not found'
        }
      case 500:
        return {
          status: err.status,
          message: 'It looks like Internal Server Error'
        }
      case 503:
        return {
          status: err.status,
          message: 'It looks like the Web Server isn’t available'
        }
      case 504:
        return {
          status: err.status,
          message: 'It looks like Gateway Timeout'
        }
      }
    }
  }
}
