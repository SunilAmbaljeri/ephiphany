import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchieveDocumentComponent } from './archieve-document.component';

describe('ArchieveDocumentComponent', () => {
  let component: ArchieveDocumentComponent;
  let fixture: ComponentFixture<ArchieveDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchieveDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchieveDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
