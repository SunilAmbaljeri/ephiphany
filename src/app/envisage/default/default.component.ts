import { Component, OnInit, ViewChild, ElementRef, Compiler } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { DefaultService } from '../../utils/providers/default/default.service';
import { CommonService } from '../../utils/providers/common/common.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  base_url: string = environment.apiEndPoint;
  // base_url_od: string = environment.apiEndPointOd;
  @ViewChild('myInput') myInput: ElementRef;
  @ViewChild('csvInput') csvInput: ElementRef;
  @ViewChild('preProcessingDropDown') preProcessingDropDown: ElementRef;
  @ViewChild('docTypeDropDown') docTypeDropDown: ElementRef;
  @ViewChild('objectInput') objectInput: ElementRef;
  imgDisplay: boolean = true;
  loading: boolean = true;
  btnDisabled: boolean = true;
  btnClear: boolean = true;
  defaultTextArea: boolean = false;
  isCSVDisable: boolean = true;
  isOCR: boolean = true;
  extractedContent: string;
  extractedText = '';
  objectText: any;
  result = 'Result...';
  defaultOption = 'documents';
  freeText = '';
  fileName: any;
  fileString: any;
  filter: boolean;
  path: string;
  preProcessingItems: any;
  preProcessingSelectedValue: any = '';
  reportTypeItems: any;
  reportTypeSelectedValue: any = '';
  imageResult: any;
  outputFileName: any;
  downloadableFileName: any;
  objAnalyzer = 'General_Object';
  valid: boolean;
  objInputError: string;
  croppedContent: any;

  constructor(private router: Router,
    private notify: NotificationsService,
    private defaultService: DefaultService,
    private commonService: CommonService,
    private _compiler: Compiler, private _sanitizer: DomSanitizer) {
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  ngOnInit() {
    // get items for preprocessing dropdown
    this.defaultService.getPreprocessing().subscribe(
      items => this.preProcessingItems = items
    );

    // get items for report type dropdown
    this.defaultService.getReportType().subscribe(
      reportItems => this.reportTypeItems = reportItems
    );

  }

  onCheckChange(ev) {
    this.filter = ev;
  }

  selectPreprocessing(event) {
    this.preProcessingSelectedValue = event.target.value;
  }

  selectReportType(event) {
    this.selectReportType = event.target.value;

  }
  /**
 * convertFile() - convert the file into base64 string format
 * @param event file data
 */
  convertFile(event: any) {
    this.extractedContent = '';
    this.path = '';

    // let len = event.target.files[0].name.split('.').length;

    if (event.target.files && event.target.files[0]) {
      this.fileName = event.target.files[0].name;
      this.fileString = event.target.files;
      this.commonFunctionality(false, false, 'Result...');
      // const reader = new FileReader();
      // reader.onload = (event: any) => {
      //   const binaryString = event.target.result;
      //   this.fileString = binaryString.substring(binaryString.indexOf('base64,') + 7);
      // };
      // reader.readAsDataURL(event.target.files[0]);
    } else {
      this.commonFunctionality(true, true, 'Result...');
    }

  }

  convertCSVFile(event: any) {
    const len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] === 'csv') {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        const reader = new FileReader();
        reader.onload = (event: any) => {
          const binaryString = event.target.result;
          this.fileString = binaryString.substring(binaryString.indexOf('base64,') + 7);
        };
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please use only csv file');
    }
  }

  convertObjFile(event: any) {
    const ftype = event.target.files[0].type;
    if (ftype === 'image/png' || ftype === 'image/jpeg') {
      this.valid = true;
      this.commonFunctionality(false, false, 'Result...');
      this.objInputError = '';
    } else {
      this.valid = false;
      this.objInputError = 'Invalid file format';
      this.fileString = '';
      this.commonFunctionality(true, true, 'Result...');
    }
    if (this.valid) {
      this.fileString = event.target.files;
      this.fileName = event.target.files[0].name;
    }
  }

  /**
   * To get type of object-detection
   * @param selected object detection radio button input
   */
  getObjectAnalyzer(selected: string) {
    this.objAnalyzer = selected;
    this.extractedContent = '';
    this.extractedText = '';
    this.croppedContent = undefined;
    this.objectText = undefined;
  }

  /**
   * analysis() - compare with defaultOption freetext/documents based on that,
   * it wil create a defaultObj and url for the freetext and archieve test,
   * when analysis processed and get the refId as response, it will move to dashboard
   * @param val - string 
   */
  analysis(val: string) {
    let defaultObj = {};
    let url: string;
    this.loading = true;
    if (this.fileName === undefined || this.fileName === '') {
      this.notify.error('', 'Please select image');
      this.loading = false;
      return;
    }
    if (this.defaultOption === 'documents') {
      url = 'envisage/ocr/';
      defaultObj = {
        'filename': this.fileString,
        'service': 'azure',
        'multipart': true
      };
      // 'option': this.preProcessingSelectedValue,
      // 'report_type': this.selectReportType,
      // 'filename': this.fileName
    } else if (this.defaultOption === 'archieve') {
      url = 'licenseplate';
      defaultObj = {
        'content': this.fileString,
        'filename': this.fileName,
      };
    } else if (this.defaultOption === 'objdetection') {
      defaultObj = {
        'content': this.fileString,
        'option': this.objAnalyzer,
      };
      url = 'objectfile';
    }
    if (url === 'licenseplate') {
      // Multi-angle image processing
      this.defaultService.imageOCR(defaultObj, url).subscribe(res => {
        if (res) {
          this.path = '';
          this.extractedContent = '';
          this.extractedText = '';
          this.path = 'lpr';
          let imgname = res.picture.split('/')[5];
          this.extractedContent = this.base_url + 'assets/lpr/' + res.picture;
          this.extractedText = res.text;
          /* Enable this if base64 image getting from response*/
          // this.result = 'Predicted:' + ' ' + JSON.stringify(res.result).toUpperCase().replace(/"/g, '');
          // this.imageResult=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          // + res.picture);
          // this.extractedContent=this.imageResult;
          this.outputFileName = res.download_file_name;
        }
        this.loading = false;
      }, error => {
        if (error.status === 504 && error.action === 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      });
    } else if (url === 'objectfile') {
      // Object detection process
      // console.log('this.fileString', this.fileString);
      this.defaultService.objectAnalysis(defaultObj, this.fileString).subscribe(res => {
        this._compiler.clearCache();
        // this.path = '';
        this.extractedContent = null;
        this.extractedText = '';
        // this.path = 'ocr';
        setTimeout(() => {
          if (this.fileString) {
            const reader = new FileReader();
            reader.onload = (load: any) => {
              this.extractedContent = load.target.result;
            };
            reader.readAsDataURL(this.fileString[0]);
          }
          this.loading = false;
          let data;
          if (res.logo && res.num_logo > 0) {
            data = res.logo;
            if (res.num_logo > 2) {
              data = data.splice(0, 2);
            }
            data.map(lg => {
              lg.img_path = `${this.base_url}media/envisage/logo/cropped/${lg.img_path}`;
              // console.log(' lg.img_path:-', lg.img_path);
            });
            console.log('data', data);
            this.croppedContent = data;
          } else if (res.signatures && res.num_signatures > 0) {
            data = res.signatures;
            if (res.num_signatures > 2) {
              data = data.splice(0, 2);
            }
            data.map(sg => {
              // sg.img_path = `${this.base_url}${sg.img_path}`;
              sg.img_path = `${this.base_url}media/envisage/signature/cropped/${sg.img_path}`;
              console.log(' sg.img_path:-', sg.img_path);
            });
            this.croppedContent = data;
          } else if (res.objects && res.no_of_objects > 0) {
            this.objectText = res.objects;
            this.croppedContent = undefined;
          }
        }, 1000);
      }, error => {
        if (error.status === 504 && error.action === 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      });
    } else {
      // OCR image processing
      this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
        this._compiler.clearCache();
        this.path = '';
        this.extractedContent = null;
        this.extractedText = '';
        this.path = 'ocr';
        // this.extractedContent = null;
        setTimeout(() => {
          // let imgname = res.picture.split('/')[5];
          // this.extractedContent = this.base_url + 'assets/ocr/' + res.picture;
          this.extractedText = res.text;
          if (this.fileString) {
            const reader = new FileReader();
            reader.onload = (load: any) => {
              this.extractedContent = load.target.result;
            };
            reader.readAsDataURL(this.fileString[0]);
          }
          this.loading = false;
          // this.imageResult = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
          //   + res.picture);
          // this.outputFileName = res.download_file_name;
        }, 1000);
        // this.extractedContent = res.picture;
      }, error => {
        if (error.status === 504 && error.action === 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      });
    }
  }

  downloadReport(val: string) {
    let defaultObj = {};
    let url: string;
    this.loading = true;
    defaultObj = {
      'filename': this.outputFileName
    };
    this.defaultService.downloadReport(defaultObj).subscribe(res => {
      if (res) {
        this.downloadableFileName = res.filename;
        this.downloadFile(res.filedata);
      }
      this.loading = false;
    }, error => {
      if (error.status === 504 && error.action === 1) {
        this.notify.error('', error.message);
      }
      this.loading = false;
    });
  }
  downloadFile(data: any) {
    if (data) {
      var blob = this.base64ToBlob(data, 'text/plain');
      // const blob = new Blob([data], {
      // type: '' + data,
      // });
      const url = window.URL.createObjectURL(blob);
      const anchor = document.createElement('a');
      anchor.download = this.downloadableFileName;
      anchor.href = url;
      anchor.click();
    }

  }

  public base64ToBlob(b64Data, contentType = '', sliceSize = 512) {
    b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
    let byteCharacters = atob(b64Data);
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      let byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  /**
   * restrict() - to disabled/enabled the buttons based 
   * on the freeText content length
   * @param ev 
   */
  restrict(ev: any) {
    if (this.freeText.length < 1) {
      this.commonFunctionality(true, true, 'Result...');
    } else {
      this.commonFunctionality(false, false, 'Result...');
    }
  }

  defaultDisable(val: string) {
    this.defaultOption = val;
    if (this.defaultOption === 'documents') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isOCR = true;
      // this.path = '';
      // this.extractedText = '';
      // this.extractedContent = '';
      // this.croppedContent = undefined;
      // this.objectText = undefined;
    } else if (this.defaultOption === 'archieve') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isOCR = false;
      // this.path = '';
      // this.extractedContent = '';
      // this.extractedText = '';
      // this.croppedContent = undefined;
      // this.objectText = undefined;
    } else if (this.defaultOption === 'objdetection') {
      this.defaultTextArea = false;
      this.isCSVDisable = true;
      this.isOCR = false;
      // this.path = '';
      // this.extractedContent = '';
      // this.extractedText = '';
      // this.croppedContent = undefined;
      // this.objectText = undefined;
      // this.objAnalyzer = 'General_Object';
    // } else {
    //   this.defaultTextArea = false;
    //   this.isCSVDisable = true;
    //   this.isOCR = false;
      // this.path = '';
      // this.extractedContent = '';
      // this.extractedText = '';
      // this.croppedContent = undefined;
      // this.objectText = undefined;
    }
    this.clear();
  }

  clear() {
    this.commonFunctionality(true, true, 'Result...');
    this.freeText = null;
    this.fileName = '';
    this.fileString = '';
    this.myInput.nativeElement.value = '';
    this.csvInput.nativeElement.value = '';
    this.objectInput.nativeElement.value = '';
    this.extractedText = '';
    this.extractedContent = '';
    this.path = '';
    this.croppedContent = undefined;
    this.objectText = undefined;
    this.objAnalyzer = 'General_Object';
    this.reset();
  }


  reset() {
    // var selectedValues=[];
    // selectedValues=["","",""];
    // window.location.reload();
    this.preProcessingDropDown.nativeElement.value = '';
    this.docTypeDropDown.nativeElement.value = '';
  }

  commonFunctionality(val1: boolean, val2: boolean, val3: string) {
    this.btnDisabled = val1;
    this.btnClear = val2;
    this.result = val3;
  }

}
