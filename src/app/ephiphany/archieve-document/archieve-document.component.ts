import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { ArchieveService } from '../../utils/providers/archieve/archieve.service';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';

@Component({
  selector: 'app-archieve-document',
  templateUrl: './archieve-document.component.html',
  styleUrls: ['./archieve-document.component.css']
})
export class ArchieveDocumentComponent implements OnInit, AfterViewInit {
  analytics: boolean = false;
  loading: boolean = true;
  // archieve: string = 'Select a File';
  result: string = 'Results...'
  fileName: any;
  lines: any;
  fileString: any;
  archieve: string;

  /**
  *This is invoked when Angular creates a component or directive by calling new on the class.
  * @param archieveService is for ArchieveService, 
  * @param notify is for Notification( success, error, warning) show or hide 
  */
  constructor(private router: Router,
     private archieveService: ArchieveService,
    private notify: NotificationsService,
    private elementRef: ElementRef) {
    setTimeout(()=>{
      this.loading = false;
    },1000)
   }

  ngOnInit() {
   
  }

  
  ngAfterViewInit(){
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
 }

/**
 * convertFile() - convert the file into base64 string format
 * @param event 
 */
  convertFile(event: any) {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // this.archieve = this.fileName;
        var reader = new FileReader();    
        reader.onload = (event: any) => {
          var binaryString = event.target.result;
          this.lines = binaryString;
          this.fileString =binaryString.substring(binaryString.indexOf("base64,")+7);
        }
        reader.readAsDataURL(event.target.files[0]);
      }
  }
/**
 * analyz() is used for analyze the data
 */
  analyz(){
    if(this.fileName == '' && this.fileString == ''){
      this.notify.warn('', 'Select File to process');
    } else {
    this.loading = true;
    let type = this.fileName.split('.').length;
    let fileObj = {};
    fileObj={
      'filename': this.fileName,
      // 'type': this.fileName.split('.')[type-1],
      'content': this.fileString
    }
    console.log("File", fileObj);
    // localStorage.removeItem('ref_id');
    // localStorage.setItem('ref_id','HI');
    this.archieveService.fileUpload(fileObj).subscribe(res => {
      this.responseMessage(res);
      // localStorage.removeItem('ref_id');
      localStorage.setItem('ref_id',res.ref_id);
    }, error => {
      console.log("Error", error);
      if(error.ok == false){
        this.loading = false;
      }
    });
  }
  }

  /**
   * clear() is used for clear the filename and result
   */
  clear(){
    this.fileName = '';
    this.fileString = '';
    this.archieve = '';
    this.result = 'Result...';
  }

  /**
  * Response and Error messages show
  */
  responseMessage(res) {
    console.log("Response", res);
    if(res.status === 'success'){
      this.notify.success('', 'File Processed Succefully');
      this.router.navigate(['/dashboard']); 
    }
  }
}
