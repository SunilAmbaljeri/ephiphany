import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { DefaultService } from '../../utils/providers/default/default.service';
import { CommonService } from '../../utils/providers/common/common.service';
import { Router } from '@angular/router';
import { SliderModule } from 'primeng/slider';
@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  @ViewChild('myInput') myInput: ElementRef;
  // @ViewChild('csvInput') csvInput: ElementRef;
  @ViewChild('audioInput') audioInput: ElementRef;
  @ViewChild('summaryInput') summaryInput: ElementRef;
  loading: boolean = true;
  btnDisabled: boolean = true;
  btnClear: boolean = true;
  defaultTextArea: boolean;
  isCSVDisable: boolean = true;
  isOCR: boolean = false;
  isSummary: boolean;
  extractedContent: string;
  extractedsummary: string;
  result = 'Result...';
  defaultOption = 'freetext';
  freeText: string;
  fileName: any;
  sumpercent: any = 10;
  fileString: any;
  docsent: string;
  sumsent: string;
  valid: boolean;
  speechAnalyzer = 'Azure';
  audioInputError: string;
  isSpeechToText: boolean;
  converted_text: string;
  time_taken: any;
  speechResponse: boolean;

  constructor(private router: Router,
    private notify: NotificationsService,
    private defaultService: DefaultService,
    private commonService: CommonService) {
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  ngOnInit() {
  }

  /**
 * convertFile() - convert the file into base64 string format
 * @param event file data
 */
  convertFile(event: any) {
    const len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] !== 'csv') {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        this.fileString = event.target.files;
        // const reader = new FileReader();
        // reader.onload = (event: any) => {
        //   const binaryString = event.target.result;
        //   this.fileString = binaryString.substring(binaryString.indexOf('base64,') + 7);
        // };
        // reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please choose other than csv file');
    }
  }

  convertAudio(event: any) {
    const ftype = event.target.files[0].type;
    if (ftype === 'audio/wav') {
      this.valid = true;
      this.commonFunctionality(false, false, 'Result...');
      this.audioInputError = '';
    } else {
      this.valid = false;
      this.audioInputError = 'Invalid file format';
      this.fileString = '';
      this.commonFunctionality(true, true, 'Result...');
    }
    if (this.valid) {
      this.fileString = event.target.files;
    }
  }

  getSpeechAnalyzer(selected: string) {
    this.speechAnalyzer = selected;
  }

  convertsummaryFile(event: any) {
    // let len = event.target.files[0].name.split('.').length;
    if (event.target.files && event.target.files[0]) {
      this.fileName = event.target.files[0].name;
      this.commonFunctionality(false, false, 'Result...');
      this.fileString = event.target.files;
      // const reader = new FileReader();
      // reader.onload = (event: any) => {
      //   const binaryString = event.target.result;
      //   this.fileString = binaryString.substring(binaryString.indexOf('base64,') + 7);
      // };
      // reader.readAsDataURL(event.target.files[0]);
    } else {
      this.commonFunctionality(true, true, 'Result...');
    }
  }

  /**
   * converting csv file to initiate corpus analysis
   * @param val input csv file
   */
  // convertCSVFile(event: any) {
  //   const len = event.target.files[0].name.split('.').length;
  //   if (event.target.files[0].name.split('.')[len - 1] === 'csv') {
  //     if (event.target.files && event.target.files[0]) {
  //       this.fileName = event.target.files[0].name;
  //       this.commonFunctionality(false, false, 'Result...');
  //       const reader = new FileReader();
  //       reader.onload = (event: any) => {
  //         const binaryString = event.target.result;
  //         this.fileString = binaryString.substring(binaryString.indexOf('base64,') + 7);
  //       };
  //       reader.readAsDataURL(event.target.files[0]);
  //     } else {
  //       this.commonFunctionality(true, true, 'Result...');
  //     }
  //   } else {
  //     this.notify.warn('', 'Please use only csv file');
  //   }
  // }

  analysisSummray(val: string) {
    let defaultObj = {};
    let url: string;
    defaultObj = {
      'file': this.fileString,
      'percent': this.sumpercent,
      'multipart': true
    };
    url = 'epiphany/summary/file/';
    this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
      this.extractedContent = res.data.text;
      this.extractedsummary = res.data.summary;
      this.docsent = 'No. of Sentences = ' + res.data.sentences_in_document;
      this.sumsent = 'No. of Sentences = ' + res.data.sentences_in_summary;
      this.result = 'Predicted:' + ' ' + JSON.stringify(res.result).toUpperCase().replace(/"/g, '');
      this.clear();
    }, error => {
      if (error.status === 504 && error.action === 1) {
        this.notify.error('', error.message);
      }
    });
  }

  /**
   * analysis() - compare with defaultOption freetext/documents based on that,
   * it wil create a defaultObj and url for the freetext and archieve test,
   * when analysis processed and get the refId as response, it will move to dashboard
   * @param val - string
   */
  analysis(val: string) {
    let valid = false;
    this.loading = true;
    let defaultObj = {};
    let url: string;

    if (this.defaultOption === 'freetext') {
      this.freeTextAnalysis(val);
    } else if (this.defaultOption === 'documents') { // || this.defaultOption === 'archieve'
    this.result = 'Result...';
    if (this.fileString[0].type === 'application/pdf') {
      url = 'epiphany/extract/pdf/';
      valid = true;
        defaultObj = {
          'pdf_file': this.fileString,
          'multipart': true
        };
      } else if (this.fileString[0].type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
        url = 'epiphany/extract/doc/';
        valid = true;
        defaultObj = {
          'doc_file': this.fileString,
          'multipart': true
        };
      } else {
        valid = false;
        this.notify.warn('', 'Please choose .pdf/.doc/.docx file');
      }
    } else if (this.defaultOption === 'speechToText') {
      url = 'epiphany/asr/';
      valid = true;
      defaultObj = {
        'speech': this.fileString,
        'service': this.speechAnalyzer === 'Azure' ? '1' : '0',
        'multipart': true
      };
    }

    if (this.defaultOption !== 'freetext' && valid) {
      this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
        if (res.refid) {
          this.loading = false;
          this.notify.success('', 'Success');
          localStorage.removeItem('refid');
          this.commonService.setRefId(res.refid);
          localStorage.setItem('refid', res.refid);
          this.router.navigate(['/epiphany/dashboard']);
        } else if (res.data.extracted_text) {
          if (val === 'spam' || val === 'infosec' || val === 'sentiment') {
            this.freeTextAnalysis(val, res.data.extracted_text);
          }
          this.extractedContent = res.data.extracted_text;
          // this.time_taken = res.time_taken;
          this.defaultTextArea = false;
          this.speechResponse = true;
          this.loading = false;
        // } else {
          // this.loading = false;
          // this.extractedContent = res.content;
          // this.extractedsummary = res.summary;
          // this.result = 'Predicted:' + ' ' + JSON.stringify(res.data.predicted_class).toUpperCase().replace(/"/g, '');
        }
      }, error => {
        this.loading = false;
        this.speechResponse = false;
        if (error.status === 504 && error.action === 1) {
          this.notify.error('', error.message);
        }
      });
    } else {
      this.loading = false;
    }
  }

  freeTextAnalysis(val: string, result?) {
    if (result) {
      this.freeText = result;
    }
    let defaultObj = {};
    let url: string;

    if (val === 'spam') {
      url = 'epiphany/spam/text/';
      defaultObj = {
        'text': this.freeText,
      };
    } else if (val === 'infosec') {
      url = 'epiphany/infosec/text/';
      defaultObj = {
        'text': this.freeText,
      };
    } else if (val === 'sentiment') {
      url = 'epiphany/sentiment/text/';
      defaultObj = {
        'text': this.freeText,
      };
    }

    this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
      this.loading = false;
      // this.extractedContent = res.content;
      // this.extractedsummary = res.summary;
      this.result = 'Predicted:' + ' ' + JSON.stringify(res.data.predicted_class).toUpperCase().replace(/"/g, '');
    });
  }

  /**
   * restrict() - to disabled/enabled the buttons based
   * on the freeText content length
   */
  restrict(ev: any) {
    if (this.freeText.length < 1) {
      this.commonFunctionality(true, true, 'Result...');
    } else {
      this.commonFunctionality(false, false, 'Result...');
    }
  }

  defaultDisable(val: string) {
    this.defaultOption = val;
    if (this.defaultOption === 'documents') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isSummary = false;
      this.isOCR = true;
      this.isSpeechToText = false;
      this.speechResponse = false;
      // } else if (this.defaultOption === 'archieve') {
      //   this.defaultTextArea = false;
      //   this.isCSVDisable = false;
      //   this.isSummary = false;
      //   this.isSpeechToText = false;
      //   this.speechResponse = false;
    } else if (this.defaultOption === 'speechToText') {
      this.isSpeechToText = true;
      this.defaultTextArea = false;
      this.isCSVDisable = false;
      this.isSummary = false;
      this.speechAnalyzer = 'Azure';
    } else if (this.defaultOption === 'summary') {
      this.defaultTextArea = false;
      this.isCSVDisable = false;
      this.isSummary = true;
      this.isSpeechToText = false;
      this.speechResponse = false;
    } else {
      this.defaultTextArea = false;
      this.isCSVDisable = true;
      this.isSummary = false;
      this.isOCR = false;
      this.isSpeechToText = false;
      this.speechResponse = false;
    }
    this.clear();
  }

  clear() {
    this.commonFunctionality(true, true, 'Result...');
    this.freeText = null;
    this.fileName = '';
    this.fileString = '';
    this.myInput.nativeElement.value = '';
    this.audioInput.nativeElement.value = '';
    // this.csvInput.nativeElement.value = '';
    this.summaryInput.nativeElement.value = '';
    this.extractedContent = '';
    this.extractedsummary = '';
    this.converted_text = '';
    this.docsent = '';
    this.sumsent = '';
    this.sumpercent = 10;
    this.speechAnalyzer = 'Azure';
  }

  commonFunctionality(val1: boolean, val2: boolean, val3: string) {
    this.btnDisabled = val1;
    this.btnClear = val2;
    this.result = val3;
  }
}
