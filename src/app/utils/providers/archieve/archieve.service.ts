import { Injectable } from '@angular/core';
import { Headers,RequestOptions } from '@angular/http';
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AppWebUrlsService } from '../../../core/providers/app-web-urls.service';
@Injectable({
  providedIn: 'root'
})
export class ArchieveService {
  private http;
  constructor(private httpclient: HttpClient,
    private appWebUrlsService: AppWebUrlsService) { 
    this.http = this.httpclient;
  }

  /**
   * fileUpload - POST method, get the analysis response
   * @param fileObj 
   */
  fileUpload(fileObj): Observable<any>{
    let body = fileObj;
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers:headers});
    return this.http.post(this.appWebUrlsService.fullUrl+'analyseall', body, options)
    .pipe(
      map(res => {
        return res
      }),
      catchError(apiError =>{
        console.log(apiError);
        return throwError(apiError);
      })
    )
  }
}
