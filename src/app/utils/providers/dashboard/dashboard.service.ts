import { Injectable } from '@angular/core';
import { Headers,RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AppWebUrlsService } from '../../../core/providers/app-web-urls.service';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardAnalysisService {

  private http;
  constructor(private httpclient: HttpClient,
    private commonService: CommonService,
    private appWebUrlsService: AppWebUrlsService) { 
    this.http = this.httpclient;
  }

  /**
   * getDashboardData() - GET method to get the processed analysis
   */
  getDashboardData(){
    let val = this.commonService.refId;
    console.log('val', val);
    if(val === undefined){
      val = localStorage.getItem('refid');
    }
    return this.http.get(`${this.appWebUrlsService.fullUrl}ref_stats/${val}`).pipe(
      map(res => {
        return res;
      }),
      catchError(apiError =>{
        console.log(apiError);
        return throwError(apiError);
      })
    )
  }
}
