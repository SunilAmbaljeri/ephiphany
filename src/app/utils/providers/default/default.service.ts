import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AppWebUrlsService } from '../../../core/providers/app-web-urls.service';
import { ErrortransformService } from '../../../core/providers/errortransform.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DefaultService {
  private http;
  constructor(
    private httpclient: HttpClient,
    private appWebUrlsService: AppWebUrlsService,
    private errortransformService: ErrortransformService) {
    this.http = this.httpclient;
  }

  /**
 *  analysisProcess() - POST method, get the text analysis response
 * @param val Text
 */
  analysisProcess(val, url): Observable<any> {
    const body = val;
    let headers;
    let options;
    let baseurl;
    const formData: FormData = new FormData();
    if (Object.keys(val).includes('multipart')) {
      baseurl = `${this.appWebUrlsService.fullUrl}${url}`;
      headers = new Headers({ 'Content-Type': 'multipart/form-data' });
      options = new RequestOptions({ headers: headers });
      if (Object.keys(val).includes('speech')) {
        formData.append('speech', val.speech[0], val.speech[0].name);
        formData.append('service', val.service);
      } else if (Object.keys(val).includes('pdf_file')) {
        formData.append('pdf_file', val.pdf_file[0], val.pdf_file[0].name);
      } else if (Object.keys(val).includes('doc_file')) {
        formData.append('doc_file', val.doc_file[0], val.doc_file[0].name);
      } else if (Object.keys(val).includes('file')) {
        formData.append('file', val.file[0], val.file[0].name);
        formData.append('percent', val.percent);
      } else if (Object.keys(val).includes('filename')) {
        formData.append('filename', val.filename[0], val.filename[0].name);
        formData.append('service', val.service);
      }
      // body.audio_file = formData;
      return this.http.post(`${baseurl}`, formData, options)
      .pipe(
        map(res => {
          return res;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
    } else {
      baseurl = `${this.appWebUrlsService.fullUrl}${url}`;
      headers = new Headers({ 'Content-Type': 'application/json' });
      options = new RequestOptions({ headers: headers });
    return this.http.post(`${baseurl}`, body, options)
      .pipe(
        map(res => {
          return res;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }
  }
  getPreprocessing(): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.get(`${this.appWebUrlsService.fullUrl}options`, options)
      .pipe(
        map((res: any) => {
          // return res;
          return res.options;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }

  getReportType(): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.get(`${this.appWebUrlsService.fullUrl}report`, options)
      .pipe(
        map((res: any) => {
          // return res;
          return res.report;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }

  imageOCR(val, url): Observable<any> {
    const body = val;
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(`${this.appWebUrlsService.fullUrl}${url}`, body, options)
      .pipe(
        map(res => {
          return res;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }

  // Download report API call
  downloadReport(val): any {
    const body = val;
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(`${this.appWebUrlsService.fullUrl}download_file`, body, options)
      .pipe(
        map(res => {
          return res;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }

  public objectAnalysis(body: any, files?: FileList): Observable<any> {
    let url;
    if (body.option === 'General_Object') {
      url = `${this.appWebUrlsService.fullUrl}azure/object_detection/`;
    } else if (body.option === 'Signature') {
      url = `${this.appWebUrlsService.fullUrl}envisage/signature/`;
    } else if (body.option === 'Logo') {
      url = `${this.appWebUrlsService.fullUrl}envisage/logo/`;
    }
    const formData: FormData = new FormData();
    const headers = new Headers({ 'Content-Type': 'multipart/form-data' });
    const options = new RequestOptions({ headers: headers });
    formData.append('file', files[0], files[0].name);
    body.image = formData;
    // ${this.appWebUrlsService.fullUrl}
    console.log('url', url);
    return this.http.post(`${url}`, formData, options)
      .pipe(
        map(res => {
          console.log('obj detection res', res);
          return res;
        }),
        catchError(apiError => {
          const transformedError = this.errortransformService.transformError(apiError);
          return Observable.throw(transformedError);
        })
      );
  }
}
