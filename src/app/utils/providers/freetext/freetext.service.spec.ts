import { TestBed } from '@angular/core/testing';

import { FreetextService } from './freetext.service';

describe('FreetextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FreetextService = TestBed.get(FreetextService);
    expect(service).toBeTruthy();
  });
});
